**Setting up the environment variables**

1. NO_OF_GO_ROUTINES : Number of go routines, which we wanted to run concurrently

2. INVOCATIONS_PER_GO_ROUTINE : The number of synchronous execution in every go routine

3. RAMPUP_TIME : This is used for graceful/linear increase in traffic;
 if the RAMPUP_TIME is 10 and NO_OF_GO_ROUTINES is 10; then 1 go routine will be spawned every second. Post 10 seconds of RAMPUP_TIME, 10 concurrent go routine should be running

**Prividing authorization tokens**

We can add the list of tokens in main.go file directly, and they will be picked sequencially in every go routine


**Running the Script**

We can run the perf script by running below command in terminal
<code>sh run.sh
</code>