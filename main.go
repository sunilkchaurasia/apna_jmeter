package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"go.uber.org/ratelimit"
)

type user struct {
	userId int
	token  string
}

var (
	users                   []user
	rampUpTime              int
	noOfGoRoutines          int
	jobFeedBaseUrl          string
	invocationsPerGoRoutine int
)

func init() {
	users = []user{
		{12345, "XXXXXXXXX"},
	}
	rampUpTime, _ = strconv.Atoi(os.Getenv("RAMPUP_TIME"))
	if rampUpTime == 0 {
		rampUpTime = 10
	}
	noOfGoRoutines, _ = strconv.Atoi(os.Getenv("NO_OF_GO_ROUTINES"))
	if noOfGoRoutines == 0 {
		noOfGoRoutines = 1
	}
	invocationsPerGoRoutine, _ = strconv.Atoi(os.Getenv("INVOCATIONS_PER_GO_ROUTINE"))
	if invocationsPerGoRoutine == 0 {
		invocationsPerGoRoutine = 1
	}
	jobFeedBaseUrl = os.Getenv("JOB_FEED_BASE_URL")
	if jobFeedBaseUrl == "" {
		jobFeedBaseUrl = "https://api.staging.infra.apna.co"
	}
}

func main() {
	client := &http.Client{}
	start := time.Now()
	fmt.Printf("Starting Script Execution at time : %s\n", start.String())
	rl := ratelimit.New(noOfGoRoutines, ratelimit.Per(time.Duration(rampUpTime*int(time.Second))))
	ch := make(chan bool)
	var wg sync.WaitGroup
	wg.Add(noOfGoRoutines)
	for i := 0; i < noOfGoRoutines; i++ {
		rl.Take()
		fmt.Printf("Spwaned Go routine number : #%d\n", i)
		go httpCall(users, *client, i, ch, wg)
	}
	wg.Wait()
	close(ch)
	end := time.Now()
	fmt.Printf("Ending Script Execution at time : %s\n", end.String())
}

func httpCall(users []user, client http.Client, j int, ch chan bool, wg sync.WaitGroup) {
	defer wg.Done()
	for i := 0; i < invocationsPerGoRoutine; i++ {
		u := users[i%len(users)]
		url := fmt.Sprintf(
			"%s/jobfeed/job-feed/api/v2/user/%v/element/|feed|?feed_filter={}&children={}",
			jobFeedBaseUrl, u.userId,
		)
		method := "GET"
		req, err := http.NewRequest(method, url, nil)

		if err != nil {
			fmt.Println(err)
			return
		}
		req.Header.Add("Accept", "application/json")
		req.Header.Add("Authorization", fmt.Sprintf("token %s", u.token))
		req.Header.Add("Content-Type", "application/json")

		res, err := client.Do(req)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("API status code : %s\n", res.Status)
	}
	ch <- true
}
