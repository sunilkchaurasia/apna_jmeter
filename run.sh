export RAMPUP_TIME=1 # in seconds
export NO_OF_GO_ROUTINES=20
export INVOCATIONS_PER_GO_ROUTINE=10
# export JOB_FEED_BASE_URL=https://production/wrong_url/.apna.co
export JOB_FEED_BASE_URL=https://api.staging.infra.apna.co
go run main.go
